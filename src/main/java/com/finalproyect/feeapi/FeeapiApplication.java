package com.finalproyect.feeapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FeeapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(FeeapiApplication.class, args);
	}

}
