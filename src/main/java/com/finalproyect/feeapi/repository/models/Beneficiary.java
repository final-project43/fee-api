package com.finalproyect.feeapi.repository.models;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "beneficiary")
public class Beneficiary {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_beneficiary")
    private Integer id;

    @Column(name = "score")
    private Integer score;

}
