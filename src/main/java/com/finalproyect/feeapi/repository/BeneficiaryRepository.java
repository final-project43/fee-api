package com.finalproyect.feeapi.repository;

import com.finalproyect.feeapi.repository.models.Beneficiary;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface BeneficiaryRepository extends JpaRepository<Beneficiary, Integer> {

    Optional<Beneficiary> findById(Integer id);
}
