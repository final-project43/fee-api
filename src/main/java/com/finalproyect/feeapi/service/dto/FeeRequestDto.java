package com.finalproyect.feeapi.service.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class FeeRequestDto {


    private Integer clientId;


    private BigDecimal amountUsd;
}
