package com.finalproyect.feeapi.service.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.math.BigDecimal;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BalanceResponse {

    private Integer id;

    private BigDecimal amount;

    private BigDecimal finalAmount;

    private BigDecimal fee;

    private String error;

}
