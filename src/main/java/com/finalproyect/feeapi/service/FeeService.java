package com.finalproyect.feeapi.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.finalproyect.feeapi.repository.BeneficiaryRepository;
import com.finalproyect.feeapi.repository.models.Beneficiary;
import com.finalproyect.feeapi.service.dto.BalanceResponse;
import com.finalproyect.feeapi.service.dto.FeeRequestDto;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class FeeService {

   private final BeneficiaryRepository beneficiaryRepository;

   @Autowired
   public FeeService(BeneficiaryRepository feeRepository) {
      this.beneficiaryRepository = feeRepository;
   }

   public ResponseEntity<BalanceResponse> calculateFee(FeeRequestDto feeRequestDto) {
      Optional<Beneficiary> optionalBeneficiary = beneficiaryRepository.findById(feeRequestDto.getClientId());
      if (optionalBeneficiary.isPresent()) {
         log.info("Calculating fee for customer " + feeRequestDto.getClientId());
         Beneficiary beneficiary = optionalBeneficiary.get();
         int percentage = calculatePercentage(beneficiary);
         BigDecimal amount = feeRequestDto.getAmountUsd();
         BigDecimal fee = amount.multiply(new BigDecimal(percentage)).divide(new BigDecimal(100), RoundingMode.DOWN);
         BigDecimal finalAmount = amount.subtract(fee).setScale(2, RoundingMode.DOWN);
         BalanceResponse balanceResponse = new BalanceResponse();
         balanceResponse.setId(feeRequestDto.getClientId());
         balanceResponse.setFinalAmount(finalAmount);
         balanceResponse.setFee(fee);
         return ResponseEntity.ok(balanceResponse);
      } else {
         BalanceResponse balanceResponse = new BalanceResponse();
         balanceResponse.setError("Client {" + feeRequestDto.getClientId() + "} not found");
         return new ResponseEntity<>(balanceResponse, HttpStatus.NOT_FOUND);
      }

   }

   private Integer calculatePercentage(Beneficiary beneficiary) {
      int percentage = 4;
      int score = beneficiary.getScore();
      if (score >= 1 && score <= 10) {
         return percentage;
      }
      if (score >= 11 && score <= 20) {
         percentage = 3;
         return percentage;
      }
      if (score >= 21 && score <= 30) {
         percentage = 2;
         return percentage;
      }
      if (score >= 31) {
         percentage = 1;
         return percentage;
      } else {
         return percentage;
      }
   }

}
