package com.finalproyect.feeapi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.finalproyect.feeapi.service.FeeService;
import com.finalproyect.feeapi.service.dto.BalanceResponse;
import com.finalproyect.feeapi.service.dto.FeeRequestDto;

@RestController
@RequestMapping

public class FeeController {

   private final FeeService feeService;

   @Autowired
   public FeeController(FeeService feeService) {
      this.feeService = feeService;
   }

   @PostMapping("/feeCalculator")
   public ResponseEntity<BalanceResponse> calculateFee(@RequestBody FeeRequestDto feeRequestDto) {
      return feeService.calculateFee(feeRequestDto);
   }

}
