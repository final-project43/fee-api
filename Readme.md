# Fee API

Api encargada de calcular las comisiones de las extracciones de dinero según el score del cliente (hardcoded).

## Flujo de cálculo de comisiones
1. El cliente solicita calcular la comisión del monto a retirar en USD.
2. El sistema verifica que el cliente exista.
3. El sistema calcula la comisión sobre el monto según el score del cliente.
4. El sistema resta al monto inicial la comisión correspondiente.
5. El sistema retorna el monto total a recibir del retiro (USD) y además expresa el monto antes del retiro y la comisión aplicada.


## Cálculo de fee según score

El sistema se basará en el score crediticio del cliente para determinar si le corresponde mas o menos comisión


Valores de score:

- **De 1 a 10:** fee 4%.
- **De 11 a 20:** fee 3%
- **De 21 a 30:** fee 2%
- **De 31 a 40:** fee 1%

## Endpoints

Se utiliza para calcular la comisión que le corresponde a un cliente sobre el monto que desea extraer. La comisión se debe calcular sobre el monto a extraer, es decir, si extraigo 100 y la comisión es de 5 que el monto final sea 95.

`[POST] /feeCalculator`

#### Body:

```json
{
  "clientId": 123,
  "amountUsd": 12354.50
}
```

#### Response:

code : 200

```json
{
  "initialAmount": 1000.00,
  "finalAmount": 996.00,
  "fee": 4
}
```

Code: 404

``` json
{
    "error": "Client {clientId} not found."
}
```